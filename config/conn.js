const mysql = require('mysql')
const conn = mysql.createConnection({
  host     : 'localhost',
  port     : 3306,
  user     : 'root',
  password : '',
  database : 'mibase'
});
 
conn.connect(function(err) {
    if(err) throw err;
    console.log("Conectado")
})

module.exports = conn
 
// connection.query('SELECT 1 + 1 AS solution', function (error, results, fields) {
//   if (error) throw error;
//   console.log('The solution is: ', results[0].solution);
// });
 
// connection.end();